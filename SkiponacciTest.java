import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SkiponacciTest {
    @Test
    public void testSum() {
        SkiponacciSequence skip = new SkiponacciSequence(1,2,3);
        int i = skip.getTerm(1);
        assertEquals(3, i);
    }
}
