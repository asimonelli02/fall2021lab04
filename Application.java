import java.util.Scanner;
public class Application {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter sequences (Format: 'Fib;1;1;x;Skip;2;2;3'");
        String sequenceInput = input.next();
        Sequence[] sequence = parse(sequenceInput);

        print(sequence, 10);
    }

    public static void print(Sequence[] sequence, int n) {
        for (int i=0; i<sequence.length; i++) {
            for (int j=0; j<n; j++) {
                System.out.println(sequence[i].getTerm(j));
            }
        }
    }

    public static Sequence[] parse(String input){
        String[] num = input.split(";");
        Sequence[] seq = new Sequence[num.length/4];
        for(int i = 0; i<num.length; i++){
            if(num[i] == "Fib"){
                seq[i] = new FibonacciSequence(Integer.parseInt(num[i+1]), Integer.parseInt(num[i+2]));
            }
            else if(num[i] == "Skip"){
                seq[i] = new SkiponacciSequence(Integer.parseInt(num[i+1]), Integer.parseInt(num[i+2]), Integer.parseInt(num[i+3]));
            }
        }
        return seq;
    }
}
