public class SkiponacciSequence extends Sequence{
    protected int first;
    protected int second;
    protected int third;

    public SkiponacciSequence(int first, int second, int third){
        this.first = first;
        this.second = second;
        this.first = third;
    }

    public int getTerm(int n){
        if(n == 1){
            return first;
        }
        if(n == 2){
            return second;
        }
        if(n == 3){
            return third;
        }
        for(int i = 0; i < n; i++){
            n = first + second;
            first = second;
            second = third;
            third = n;
        }
        return n;
    }
}