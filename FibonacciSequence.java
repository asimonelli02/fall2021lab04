public class FibonacciSequence extends Sequence {
    private int first;
    private int second;

    public FibonacciSequence(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getTerm(int n) {
        if (n == first) {
            return first;
        }
        if (n == second) {
            return second;
        }
        n = first + second;
        return n;
    }
}
 