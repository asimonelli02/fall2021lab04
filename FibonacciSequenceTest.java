import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class FibonacciSequenceTest {
    @Test
    public void getTermTest(){
        FibonacciSequence seq = new FibonacciSequence(2, 3);
        assertEquals(8, seq.getTerm(4));
    }
}
